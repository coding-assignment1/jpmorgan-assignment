# JPMorgan-Assignment

### Steps to run this project

### `npm install`
Run this command to install required node packages for the project.

### `npm start`
Run this command to start local server and run the project at port 3000. If port 3000 is busy it will start the project at next available port.
