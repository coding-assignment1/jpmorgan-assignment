// import { useEffect, useState } from 'react';
import MainLineChartComponent from './components/MainLineChartComponent';
import SummaryLineChartComponent from './components/SummaryLineChartComponent';
import './resources/App.css';
// const jsonData = require('./resources/data')
import {lables,data} from './resources/data'

function App() {
	// debugger;
	const chartData = {lables,data};
//   const [chartData, setChartData] = useState({lables,data});

//   useEffect(() => {setChartData(...chartData, data)},[data]);

  let handleChartOnDrag = (event) => {
    // debugger;
    // fetch latest chart data and set it to chartData object.
  }

  return (
    <>
		<MainLineChartComponent chartData={chartData}/>
		<SummaryLineChartComponent chartData={chartData} handleChartOnDrag = {() => handleChartOnDrag}/>
    </>
    
  );
}

export default App;
