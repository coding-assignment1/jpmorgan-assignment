import React from 'react'
import PropTypes from 'prop-types'
import { Line } from 'react-chartjs-2'


function SummaryLineChartComponent(props) {
	// debugger;
	let data = props.chartData;
	// let data = [10,20,30,40]
	// console.log(`FinalData : ${data}`);
	return (
		<div>
			{/* This is Summary Line Chart Component. */}
			{data !== null ? <Line 
			data={{
				labels:data.lables,
				datasets:[{
					label: "Name",
					data:data.data,
					backgroundColor: [
						'rgba(255, 255, 255, 255)',
					// 	'rgba(54, 162, 235, 0.2)',
					// 	'rgba(255, 206, 86, 0.2)',
					// 	'rgba(75, 192, 192, 0.2)',
					// 	'rgba(153, 102, 255, 0.2)',
					// 	'rgba(255, 159, 64, 0.2)'
					],
					borderColor: [
						'rgba(137, 196, 244, 1)',
						// 'rgba(255, 99, 132, 1)',
						// 'rgba(54, 162, 235, 1)',
						// 'rgba(255, 206, 86, 1)',
						// 'rgba(75, 192, 192, 1)',
						// 'rgba(153, 102, 255, 1)',
						// 'rgba(255, 159, 64, 1)'
					],
				}]
			}}
			options={{
				title: {
					display: true,
					text: "Summary Line Chart"
				},
				maintainAspectRatio: true,
				scales: {
					yAxes: [{
						display:false
					}],
					xAxes:[{
						display:false
					}]
				},
				tooltips:{
					enabled:false
				},
				events:['click'],
				onClick: function(event){
					props.handleChartOnDrag();
					// debugger;
					// event.mousedown();
				},
			}}
			/> : null}
			
		</div>
	)
}

SummaryLineChartComponent.propTypes = {
	chartData: PropTypes.object,
	handleChartOnDrag: PropTypes.func
}

export default SummaryLineChartComponent