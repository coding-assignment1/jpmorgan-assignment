import React from 'react'
import PropTypes from 'prop-types'
import { Line } from 'react-chartjs-2'

function MainLineChartComponent(props) {
    let data = props.chartData;
    return (
        <div>
            {/* Percentage Value vs Category. */}
            <Line data={{
				labels:data.lables,
				datasets:[{
					label: "Name",
					data:data.data,
					backgroundColor: [
						'rgba(255, 255, 255, .2)',
						// 'rgba(54, 162, 235, 0.2)',
						// 'rgba(255, 206, 86, 0.2)',
						// 'rgba(75, 192, 192, 0.2)',
						// 'rgba(153, 102, 255, 0.2)',
						// 'rgba(255, 159, 64, 0.2)'
					],
					borderColor: [
						'rgba(137, 196, 244, 1)',
						// 'rgba(54, 162, 235, 1)',
						// 'rgba(255, 206, 86, 1)',
						// 'rgba(75, 192, 192, 1)',
						// 'rgba(153, 102, 255, 1)',
						// 'rgba(255, 159, 64, 1)'
					],
					}]
				}}
				options={{
					title: {
						display: true,
						text: "Percentage Value vs Category"
					  },
					maintainAspectRatio: true,
					scales: {
						yAxes: [{
							ticks: {
								min:0,
								beginAtZero: true,
								callback: function (value) {
									return (value / 100 * 100).toFixed(0) + '%'; // convert it to percentage
								  },
							}
						}],
						xAxes:[{
							ticks:{
								beginAtZero:true
							}
						}]
					},
					tooltips:{
						mode: "index",
						intersect: true
					// 	enabled:false,
					// 	custom: function(tooltipModel) {
					// 		// Tooltip Element
					// 		var tooltipEl = document.getElementById('chartjs-tooltip');
			
					// 		// Create element on first render
					// 		if (!tooltipEl) {
					// 			tooltipEl = document.createElement('div');
					// 			tooltipEl.id = 'chartjs-tooltip';
					// 			tooltipEl.innerHTML = '<table></table>';
					// 			document.body.appendChild(tooltipEl);
					// 		}
			
					// 		// Hide if no tooltip
					// 		if (tooltipModel.opacity === 0) {
					// 			tooltipEl.style.opacity = 0;
					// 			return;
					// 		}
			
					// 		// Set caret Position
					// 		tooltipEl.classList.remove('above', 'below', 'no-transform');
					// 		if (tooltipModel.yAlign) {
					// 			tooltipEl.classList.add(tooltipModel.yAlign);
					// 		} else {
					// 			tooltipEl.classList.add('no-transform');
					// 		}
			
					// 		function getBody(bodyItem) {
					// 			return bodyItem.lines;
					// 		}
			
					// 		// Set Text
					// 		if (tooltipModel.body) {
					// 			var titleLines = tooltipModel.title || [];
					// 			var bodyLines = tooltipModel.body.map(getBody);
			
					// 			var innerHtml = '<thead>';
			
					// 			titleLines.forEach(function(title) {
					// 				innerHtml += '<tr><th>' + title + '</th></tr>';
					// 			});
					// 			innerHtml += '</thead><tbody>';
			
					// 			bodyLines.forEach(function(body, i) {
					// 				var colors = tooltipModel.labelColors[i];
					// 				var style = 'background:' + colors.backgroundColor;
					// 				style += '; border-color:' + colors.borderColor;
					// 				style += '; border-width: 2px';
					// 				var span = '<span style="' + style + '"></span>';
					// 				innerHtml += '<tr><td>' + span + body + '</td></tr>';
					// 			});
					// 			innerHtml += '</tbody>';
			
					// 			var tableRoot = tooltipEl.querySelector('table');
					// 			tableRoot.innerHTML = innerHtml;
					// 		}
			
					// 		// `this` will be the overall tooltip
					// 		var position = this._chart.canvas.getBoundingClientRect();
			
					// 		// Display, position, and set styles for font
					// 		tooltipEl.style.opacity = 1;
					// 		tooltipEl.style.position = 'absolute';
					// 		tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX + 'px';
					// 		tooltipEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY + 'px';
					// 		tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
					// 		tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
					// 		tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
					// 		tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
					// 		tooltipEl.style.pointerEvents = 'none';
					// 	}
					}
				}}
			/>
        </div>
    )
}

MainLineChartComponent.propTypes = {
    chartData: PropTypes.object
}

export default MainLineChartComponent



